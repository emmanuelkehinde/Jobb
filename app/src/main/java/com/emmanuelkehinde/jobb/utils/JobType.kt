package com.emmanuelkehinde.jobb.utils

enum class JobType(val value: String) {
    FULL_TIME("Full Time"),
    PART_TIME("Part Time"),
    REMOTE("Remote"),
    CONTRACT("Contract")
}