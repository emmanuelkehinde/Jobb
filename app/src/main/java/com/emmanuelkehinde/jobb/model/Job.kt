package com.emmanuelkehinde.jobb.model

import android.os.Parcelable
import com.google.firebase.firestore.ServerTimestamp
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Job(
        var jobId: String = "",
        var userId: String = "",
        var jobTitle: String = "",
        var jobDescription: String = "",
        var jobType: String = "",
        var jobSalary: String = "",
        @ServerTimestamp var datePosted: Date? = null
) : Parcelable
