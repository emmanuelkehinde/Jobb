package com.emmanuelkehinde.jobb.views.partials

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.emmanuelkehinde.jobb.model.Job
import com.github.marlonlom.utilities.timeago.TimeAgo
import kotlinx.android.synthetic.main.layout_job_post.view.*
import java.util.*

class JobHolder(itemView: View, private var jobPostListener: JobPostListener): RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private lateinit var model: Job

    interface JobPostListener {
        fun onJobPostClicked(job: Job)
    }

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        jobPostListener.onJobPostClicked(model)
    }

    fun bind(model: Job) {
        this.model = model
        setTitle(model.jobTitle)
        setType(model.jobType)
        setDate(model.datePosted)
    }

    private fun setTitle(jobTitle: String) {
        itemView.txt_job_title.text = jobTitle
    }

    private fun setType(jobType: String) {
        itemView.txt_job_type.text = jobType
    }

    private fun setDate(datePosted: Date?) {
        datePosted?.time?.let {
            itemView.txt_date_posted.text = TimeAgo.using(it)
        }
    }

}