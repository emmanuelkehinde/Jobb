package com.emmanuelkehinde.jobb.views

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.emmanuelkehinde.jobb.R
import com.emmanuelkehinde.jobb.model.Job
import com.emmanuelkehinde.jobb.utils.Constants
import kotlinx.android.synthetic.main.activity_job_detail.*

class JobDetailActivity : AppCompatActivity() {

    private var userId: String? = ""
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent?.getParcelableExtra<Job>(Constants.JOB_EXTRA) != null) {
            job = intent.getParcelableExtra(Constants.JOB_EXTRA)!!
            txt_job_title.text = job.jobTitle
            txt_job_type.text = job.jobType

            val salary = "$${job.jobSalary}"
            txt_job_salary.text = salary
            txt_job_desc.text = job.jobDescription
            userId = job.userId
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_job_detail,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_apply -> {
                val intent = Intent(Intent.ACTION_SENDTO)
                intent.data = Uri.parse("mailto:$userId")
                intent.putExtra("subject",job.jobTitle)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(Intent.createChooser(intent,getString(R.string.title_msg_send_an_email)))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
