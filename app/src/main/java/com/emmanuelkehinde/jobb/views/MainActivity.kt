package com.emmanuelkehinde.jobb.views

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.*
import com.emmanuelkehinde.jobb.R
import com.emmanuelkehinde.jobb.model.Filter
import com.emmanuelkehinde.jobb.model.Job
import com.emmanuelkehinde.jobb.utils.Constants
import com.emmanuelkehinde.jobb.views.custom.EndlessRecyclerViewScrollListener
import com.emmanuelkehinde.jobb.views.partials.JobHolder
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {

    private val FILTER_REQUEST_CODE: Int = 2
    private var LIMIT: Long = 10

    private lateinit var fireStore: FirebaseFirestore
    private lateinit var adapter: FirestoreRecyclerAdapter<Job, JobHolder>
    private lateinit var linearLayout: LinearLayoutManager
    private var filter: Filter = Filter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.title_msg_job_posts)

        fab.setOnClickListener { _ ->
            startActivity(Intent(this, AddJobPostActivity::class.java))
        }

        fireStore = FirebaseFirestore.getInstance()
        val fireStoreSettings = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true)
        fireStore.firestoreSettings = fireStoreSettings.build()

        linearLayout = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rec_job_posts.addOnScrollListener(object : EndlessRecyclerViewScrollListener(linearLayout) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                LIMIT += 10
                item_progress_bar.visibility = View.VISIBLE
                onStart()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_filter -> {
                startActivityForResult(Intent(this, JobFilterActivity::class.java), FILTER_REQUEST_CODE)
                true
            }
            R.id.action_clear_filters -> {
                filter = Filter()
                onStart()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        rec_job_posts.layoutManager = linearLayout
        rec_job_posts.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rec_job_posts.adapter = getJobAdapter(filter)

        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

    private fun getJobAdapter(filter: Filter): FirestoreRecyclerAdapter<Job, JobHolder> {
        val query: Query = constructQuery(filter)

        val options = FirestoreRecyclerOptions.Builder<Job>()
                .setQuery(query, Job::class.java)
                .build()
        adapter = object : FirestoreRecyclerAdapter<Job, JobHolder>(options), JobHolder.JobPostListener {
            override fun onJobPostClicked(job: Job) {
                val intent = Intent(this@MainActivity, JobDetailActivity::class.java)
                intent.putExtra(Constants.JOB_EXTRA,job)
                startActivity(intent)
            }

            override fun onBindViewHolder(holder: JobHolder, position: Int, model: Job) {
                item_progress_bar.visibility = View.GONE
                holder.bind(model)
            }

            override fun onCreateViewHolder(group: ViewGroup, i: Int): JobHolder {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                val view = LayoutInflater.from(group.context)
                        .inflate(R.layout.layout_job_post, group, false)

                return JobHolder(view, this)
            }
        }
        return adapter
    }

    private fun constructQuery(filter: Filter): Query {
        var query: Query = fireStore.collection("jobs")

        if (!TextUtils.isEmpty(filter.jobTitle)) {
            query = query.orderBy("jobTitle").whereGreaterThanOrEqualTo("jobTitle", filter.jobTitle)
        }
        if (!TextUtils.isEmpty(filter.jobSalary)) {
            query = query.orderBy("jobSalary").whereGreaterThanOrEqualTo("jobSalary", filter.jobSalary)
        }
        if (!TextUtils.isEmpty(filter.jobType)) {
            query = query.whereEqualTo("jobType", filter.jobType)
        }

        query = query.orderBy("datePosted", Query.Direction.DESCENDING)
        query = query.limit(LIMIT)
        return query
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILTER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            filter = data?.getParcelableExtra(Constants.FILTER_EXTRA)!!
        }
    }

}
