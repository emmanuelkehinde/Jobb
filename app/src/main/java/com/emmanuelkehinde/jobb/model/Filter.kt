package com.emmanuelkehinde.jobb.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Filter(
    var jobTitle: String = "",
    var jobType: String = "",
    var jobSalary: String = ""
) : Parcelable
