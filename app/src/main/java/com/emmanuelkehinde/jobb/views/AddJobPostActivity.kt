package com.emmanuelkehinde.jobb.views

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import com.google.android.material.textfield.TextInputEditText
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.emmanuelkehinde.jobb.R
import com.emmanuelkehinde.jobb.model.Job
import com.emmanuelkehinde.jobb.utils.Constants
import com.emmanuelkehinde.jobb.utils.JobType
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_add_job_post.*
import java.util.*
import kotlin.collections.HashMap


class AddJobPostActivity : AppCompatActivity() {

    private lateinit var progress: ProgressDialog
    private lateinit var fireStore: FirebaseFirestore
    private var auth: FirebaseAuth? = null

    private val RC_SIGN_IN = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_job_post)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addJobTypeContents(spn_job_type)
        fireStore = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()

        checkForAuth()
    }

    private fun addJobTypeContents(spn_job_type: Spinner?) {
        spn_job_type?.adapter = ArrayAdapter(applicationContext,android.R.layout.simple_list_item_1,
                listOf(JobType.FULL_TIME.value,JobType.PART_TIME.value,JobType.REMOTE.value,JobType.CONTRACT.value))
    }

    private fun checkForAuth() {
        if (auth?.currentUser == null) {
            Toast.makeText(this,getString(R.string.prompt_msg_sign_in_to_post_a_job),Toast.LENGTH_SHORT).show()
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(listOf(
                                    AuthUI.IdpConfig.EmailBuilder().build(),
                                    AuthUI.IdpConfig.GoogleBuilder().build())
                            )
                            .setIsSmartLockEnabled(false)
                            .setTheme(R.style.LoginTheme)
                            .build(),
                    RC_SIGN_IN)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (auth?.currentUser != null) {
            menuInflater.inflate(R.menu.menu_post_job_signed_in,menu)
        } else {
            menuInflater.inflate(R.menu.menu_post_job, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_post_job -> {
                val title = edt_job_title?.text.toString()
                val salary = edt_job_salary?.text.toString()
                val desc = edt_job_description?.text.toString()
                if (title.isEmpty() || salary.isEmpty() || desc.isEmpty()) {
                    Toast.makeText(this,getString(R.string.prompt_msg_insert_all_required_attributes),Toast.LENGTH_SHORT).show()
                } else {
                    postJob(title, spn_job_type.selectedItem.toString(), salary, desc)
                }
                true
            }
            R.id.action_sign_out -> {
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener {
                            // user is now signed out
                            finish()
                        }

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun postJob(jobTitle: String, jobType: String, salary: String, jobDescription: String) {
        showProgress(getString(R.string.progress_msg_posting_job))

        val jobDoc = fireStore.collection("jobs").document()
        val job = Job(jobDoc.id, auth?.currentUser?.email!!,jobTitle,jobDescription,jobType,salary)
        jobDoc.set(job)
                .addOnSuccessListener {
                    Toast.makeText(this,getString(R.string.success_msg_job_posted_successfully),Toast.LENGTH_SHORT).show()
                    hideProgress()
                    finish()
                }
                .addOnFailureListener {
                    exception -> Toast.makeText(this,exception.localizedMessage,Toast.LENGTH_SHORT).show()
                    hideProgress()
                }

    }

    private fun showProgress(message: String) {
        progress = ProgressDialog(this)
        progress.isIndeterminate = true
        progress.setMessage(message)
        progress.show()
    }

    private fun hideProgress() {
        progress.cancel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            // Successfully signed in
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this,getString(R.string.success_msg_signed_in),Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, AddJobPostActivity::class.java))
                finish()
                return
            } else {
                // Sign in failed
                if (response == null) {
                    finish()
                    return
                }

                if (response.error?.errorCode == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(this,getString(R.string.error_msg_no_internet_connection),Toast.LENGTH_SHORT).show()
                    finish()
                    return
                }

                if (response.error?.errorCode == ErrorCodes.UNKNOWN_ERROR) {
                    Toast.makeText(this,getString(R.string.error_msg_unknown_error_occurred),Toast.LENGTH_SHORT).show()
                    finish()
                    return
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
