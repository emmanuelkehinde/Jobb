package com.emmanuelkehinde.jobb.views

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.emmanuelkehinde.jobb.R
import com.emmanuelkehinde.jobb.model.Filter
import com.emmanuelkehinde.jobb.utils.Constants
import com.emmanuelkehinde.jobb.utils.JobType
import kotlinx.android.synthetic.main.activity_job_filter.*

class JobFilterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_filter)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addJobTypeContents(spn_job_type)
    }

    private fun addJobTypeContents(spn_job_type: Spinner?) {
        spn_job_type?.adapter = ArrayAdapter(applicationContext,android.R.layout.simple_list_item_1,
                listOf(JobType.FULL_TIME.value, JobType.PART_TIME.value, JobType.REMOTE.value, JobType.CONTRACT.value))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_filter,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_filter_done -> {
                val jobTitle = edt_job_title.text.toString()
                val jobType = spn_job_type.selectedItem.toString()
                val jobSalary = edt_job_salary.text.toString()

                val filter = Filter(jobTitle,jobType,jobSalary)
                setResult(Activity.RESULT_OK, Intent().putExtra(Constants.FILTER_EXTRA,filter))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
