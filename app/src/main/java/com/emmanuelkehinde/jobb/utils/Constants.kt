package com.emmanuelkehinde.jobb.utils

class Constants {

    companion object {
        const val JOB_EXTRA = "job_extra"
        const val FILTER_EXTRA = "filter_extra"
    }
}